from dash import dcc, html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc

# Connect to main app.py file
from app import app
from app import app

# Connect to your app pages
from apps import downloader, channelStats

navbar = dbc.NavbarSimple(
        children=[
            dbc.NavItem(dbc.NavLink("Downloader", href="/apps/downloader")),
            dbc.NavItem(dbc.NavLink("Channel statistics", href="/apps/channelStats")),
            dbc.DropdownMenu(
                children=[
                    dbc.DropdownMenuItem("More pages", header=True),
                    dbc.DropdownMenuItem("About me", href="https://karlyy.gitlab.io/portfolio/#")
                ],
                nav=True,
                in_navbar=True,
                label="More",
            ),
        ],
        brand="YouTube video downloader and analysis",
        brand_style= {'font-family': 'Capriola', 'font-style':'italic'},
        brand_href="",
        className="navbar navbar-expand-lg navbar-light bg-light lead",
        dark=False
    )

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(
        navbar,
    ),
    html.Div(id='page-content', children=[])
])


@app.callback(
    Output('page-content', 'children'),
    [Input('url', 'pathname')]

)
def display_page(pathname):
    if pathname == "/":
        return downloader.layout
    if pathname == "/apps/downloader":
        return downloader.layout
    if pathname == '/apps/channelStats':
        return channelStats.layout
    else:
        print(pathname)
        return "404 Page Error! Please choose a link"


if __name__ == '__main__':
    app.run_server(debug=False)