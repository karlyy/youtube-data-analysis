import pathlib
from tqdm import tqdm
import requests
import pandas as pd
import plotly.express as px
from dash import html, dcc
from dash.dependencies import Input, Output,State
import dash_bootstrap_components as dbc
import json
import numpy as np
#from yt_stats import YTstats
from pytube import YouTube
from urllib.parse import urlparse, parse_qs
from app import app



class YTstats:

    def __init__(self, api_key, channel_id=None):
        self.api_key = api_key
        self.channel_id = channel_id
        self.channel_statistics = None
        self.video_data = None
        self.playlist_data = None

    def get_channel_statistics(self):
        url = f'https://www.googleapis.com/youtube/v3/channels?part=statistics&id={self.channel_id}&key={self.api_key}'
        json_url = requests.get(url)
        data = json.loads(json_url.text)

        try:
            data = data['items'][0]['statistics']
        except KeyError:
            print('Could not get channel statistics')
            data = {}

        self.channel_statistics = data
        return data

    def get_channel_video_data(self):
        #get video ids
        channel_videos, channel_playlists = self._get_channel_content(limit=50)
        #get video stats
        parts=["snippet", "statistics", "contentDetails", "topicDetails"]
        for video_id in tqdm(channel_videos):
            for part in parts:
                data = self.get_single_video_data(video_id, part)
                channel_videos[video_id].update(data)
        self.video_data = channel_videos
        return channel_videos

    def get_single_video_data(self, video_id, part):
        url = f"https://www.googleapis.com/youtube/v3/videos?part={part}&id={video_id}&key={self.api_key}"
        json_url = requests.get(url)
        data = json.loads(json_url.text)  # dictionary
        try:
            data = data["items"][0][part]
        except:
            print("error")
            data = dict()
        return data

    def get_channel_playlist_data(self):
        channel_videos, channel_playlists = self._get_channel_content(limit=50)
        parts = ["contentDetails", "id", "player", "snippet", "status"]
        for playlist_id in tqdm(channel_playlists):
            for part in parts:
                if part == 'id':
                    data = {'id': self._get_single_playlist_data(playlist_id, part)}
                else:
                    data = self._get_single_playlist_data(playlist_id, part)
                channel_playlists[playlist_id].update(data)
        self.playlist_data = channel_playlists
        print(channel_playlists)
        return channel_playlists

    def _get_single_playlist_data(self, playlist_id, part):
        url = f"https://www.googleapis.com/youtube/v3/playlists?part={part}&id={playlist_id}&key={self.api_key}"

        json_url = requests.get(url)
        data = json.loads(json_url.text)  # dictionary
        try:
            data = data["items"][0][part]
        except:
            print("error")
            data = dict()
        return data

    #def _get_single_playlist_videos(self, playlist_id, part):
    #    url = f"https://www.googleapis.com/youtube/v3/playlistItems?part={part}&id={playlist_id}&key={self.api_key}"




    def _get_channel_content(self, limit=None, check_all_pages=True):
        """
        Extract all videos and playlists, can check all available search pages
        channel_videos = videoId: title, publishedAt
        channel_playlists = playlistId: title, publishedAt
        return channel_videos, channel_playlists
        """
        url = f"https://www.googleapis.com/youtube/v3/search?key={self.api_key}&channelId={self.channel_id}&part=snippet,id&order=date"
        if limit is not None and isinstance(limit, int):
            url += "&maxResults=" + str(limit)

        vid, pl, npt = self._get_channel_content_per_page(url)
        idx = 0
        while(check_all_pages and npt is not None and idx < 10):
            nexturl = url + "&pageToken=" + npt
            next_vid, next_pl, npt = self._get_channel_content_per_page(nexturl)
            vid.update(next_vid)
            pl.update(next_pl)
            idx += 1

        return vid, pl

    def _get_channel_content_per_page(self, url):
        """
        Extract all videos and playlists per page
        return channel_videos, channel_playlists, nextPageToken
        """
        json_url = requests.get(url)
        data = json.loads(json_url.text)
        channel_videos = dict()
        channel_playlists = dict()
        if 'items' not in data:
            print('Error! Could not get correct channel data!\n', data)
            return channel_videos, channel_videos, None

        nextPageToken = data.get("nextPageToken", None)

        item_data = data['items']
        for item in item_data:
            try:
                kind = item['id']['kind']
                published_at = item['snippet']['publishedAt']
                title = item['snippet']['title']
                if kind == 'youtube#video':
                    video_id = item['id']['videoId']
                    channel_videos[video_id] = {'publishedAt': published_at, 'title': title}
                elif kind == 'youtube#playlist':
                    playlist_id = item['id']['playlistId']
                    channel_playlists[playlist_id] = {'publishedAt': published_at, 'title': title}
            except KeyError as e:
                print('Error! Could not extract data from item:\n', item)

        return channel_videos, channel_playlists, nextPageToken


    def dump(self):
        if self.channel_statistics is None or self.video_data is None:
            print("Data is none")
            return

        fused_data = {self.channel_id:
                          {"channel_statistics": self.channel_statistics,
                           "video_data": self.video_data,
                           "playlist_data": self.playlist_data}
                      }

        channel_title = self.video_data.popitem()[1].get("channelTitle", self.channel_id)
        channel_title = channel_title.replace(" ", "_").lower()
        file_name = DATA_PATH.joinpath('youtube.json')
        with open(file_name, "w") as f:
            json.dump(fused_data, f, indent=4)

        print('file dumped')





# get relative data folder
PATH = pathlib.Path(__file__).parent
DATA_PATH = PATH.joinpath("../datasets").resolve()
API_KEY = "AIzaSyA96dN-uCZCCBiMURXazrhkHT5ye1GXq3k"

video_from_input =""
CHANNEL_ID = ""
#dumping file and preparing for channel stats
"""if CHANNEL_ID != "":
    y = YTstats(API_KEY, CHANNEL_ID)
    y.get_channel_statistics()
    y.get_channel_video_data()
    y.get_channel_playlist_data()
    y.dump()"""



#just for no error at beginning
dff = pd.DataFrame()
dff['viewCount'] = []
dff['commentCount'] = []



left_jumbotron = dbc.Col(
    html.Div(
        [
            html.Span("Video Id:", className="display-5"),
            html.Span(className="display-5", id='parsed_video_id'),
            html.Hr(className="my-2"),
            html.P(id='container-button-basic'),
            dbc.Button("Channel statistics", color="light", outline=True, href='/apps/channelStats'),
        ],
        className="h-100 p-5 text-white bg-dark rounded-3",
    ),
    md=6,
)

right_jumbotron = dbc.Col(
    html.Div(
        [
            html.H2("Downloader", className="display-4"),
            html.Hr(className="my-2"),
            html.Span("Download a YouTube video by your choice and look at its statistics."),
            html.Br(),
            html.Span("You can also check the statistics for its channel! "),
            html.Div([
                dcc.Input(id='input-on-submit', type='text', value="Enter video ID"),
                dbc.Button("Download", id='submit-val', className='ml-2', color="secondary", n_clicks=0, outline=True),
                html.Div(html.P('Enter a value and press submit'))
                ], className='d-inline-block'),
        ],
        className="h-100 p-5 bg-light border rounded-3",
    ),
    md=6,
)

jumbotron = dbc.Row(
    [right_jumbotron, left_jumbotron],
    className="align-items-md-stretch",
)

spinners = html.Div(
    [
        dbc.Spinner(color="primary", type="grow"),
        dbc.Spinner(color="secondary", type="grow"),
        dbc.Spinner(color="success", type="grow"),
        dbc.Spinner(color="warning", type="grow"),
        dbc.Spinner(color="danger", type="grow"),
        dbc.Spinner(color="info", type="grow"),
        dbc.Spinner(color="light", type="grow"),
        dbc.Spinner(color="dark", type="grow"),
    ]
)

layout = html.Div(children=[
    html.Div(
      id= 'container-button-basic-2'
    ),
    html.Div(
      jumbotron,
        className="my-5"
    ),
    dcc.Graph(
        id='example-graph-1',
        figure=  px.bar(data_frame=dff, x='viewCount', y= 'commentCount')
    )],
    className="container",
    #style={'background':'red'}
)

@app.callback(
    [Output('container-button-basic', 'children'),
     Output('container-button-basic-2', 'children'),
     Output('parsed_video_id', 'children'),
     Output('example-graph-1', 'figure')],
    Input('submit-val', 'n_clicks'),
    State('input-on-submit', 'value')
)

def update_output(n_clicks, value):
    #SAVE_PATH = "C:\Users\Karolina\Desktop\data mining"  # to_do
    videoId, title, channelTitle, publishedAt, viewCount, likeCount, commentCount="","","","","","",""
    statsDff = pd.DataFrame()
    figure=px.bar()
    fileDumped = False
    alert =html.Div()
    if value != "Enter video ID":
        url_data = urlparse(value)
        query = parse_qs(url_data.query)
        videoId = query["v"][0]

        try:
            yt = YouTube(value)  # object creation using YouTube
        except:
            print("Connection Error")  # to handle exception
        stream = yt.streams.first()
        #mp4files = yt.filter('mp4')   # filters out all the files with "mp4" extension
        #yt.set_filename('DownloadedVideo')      # to set the name of the file
        #d_video = yt.get(mp4files[-1].extension, mp4files[-1].resolution)   # get the video with the extension and resolution passed in the get() function
        try:
            stream.download()

            url_data = urlparse.urlparse(value)

            query = urlparse.parse_qs(url_data.query)
            videoId = query["v"][0]
            #d_video.download(SAVE_PATH)  # downloading the video
        except:
            print("Some Error!")
        print('Task Completed!')

        global video_from_input
        video_from_input = videoId
        print(video_from_input)

        ytt = YTstats(API_KEY)
        snippet = ytt.get_single_video_data(video_from_input,'snippet')
        statss = ytt.get_single_video_data(video_from_input,'statistics')

        content = ytt.get_single_video_data(video_from_input,'contentDetails')
        title=snippet['title']
        publishedAt = snippet['publishedAt']
        channelTitle = snippet['channelTitle']
        channelId = snippet['channelId']

        try:
            y = YTstats(API_KEY, channelId)
            y.get_channel_statistics()
            y.get_channel_video_data()
            y.get_channel_playlist_data()
            y.dump()
            fileDumped=True
        except:
            print("Not dumped")

        #data for the single video
        statsDff = pd.DataFrame(columns=statss.keys(), data=[statss.values()])
        statsDff = statsDff.transpose()
        statsDff['Count'] = statsDff.index
        statsDff[0] = statsDff[0].astype('int64')
        statsDff = statsDff.sort_values(0)
        print(statsDff)
        figure = px.bar(data_frame=statsDff, x='Count', y=0)
        alert = dbc.Alert("Successfully downloaded", color="success", id='container-button-basic-2', dismissable=True, is_open=True)

    return 'Title: "{}"\n Published at: {}\n Channel title: "{}"'.format(
        title,
        publishedAt,
        channelTitle
    ), alert, videoId, figure


def counter(i):
    print(i+1)

