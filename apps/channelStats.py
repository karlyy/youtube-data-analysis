import pathlib
import json
import pandas as pd
import numpy as np
from dash import html
import plotly.express as px
from dash import html, dcc
from dash.dependencies import Input, Output,State
import dash_bootstrap_components as dbc
from app import app
import plotly.figure_factory as ff
import scipy
import plotly.graph_objects as go
import datetime

# get relative data folder
PATH = pathlib.Path(__file__).parent
DATA_PATH = PATH.joinpath("../datasets").resolve()

file = DATA_PATH.joinpath('youtube.json')
data = None
with open (file, 'r') as f:
  data = json.load(f)

channel_id, stats = data.popitem()
channel = stats["channel_statistics"]
videos = stats["video_data"]
playlists = stats["playlist_data"]
ch = pd.DataFrame.from_dict(channel.items())
channel_df =pd.DataFrame(columns=ch[0], data=[np.array(ch[1])])
vid = pd.DataFrame(data=videos)
videos_df = vid.transpose()
videos_df = videos_df.drop(['localized'], axis=1)
pl = pd.DataFrame(data=playlists)
playlists_df = pl.transpose()
playlists_df = playlists_df.drop(['localized'], axis=1)



strValues = ['categoryId', 'viewCount', 'likeCount', 'favoriteCount', 'commentCount']
for val in strValues:
  videos_df[val] = videos_df[val].astype('int64')

sorted_videos_by_likes = videos_df.sort_values(by=['likeCount'], ascending=False)
#dff = sorted_videos_by_likes.iloc[0:5, :]
size = None
print()
if sorted_videos_by_likes["commentCount"].all() > 0:
  size = 'commentCount'
else:
  size = None
fig = px.scatter(sorted_videos_by_likes, x="likeCount", y="viewCount", size=size, color="title",
           hover_name="title", log_x=True, size_max=60)



"""ch = ch[ch.index != 2]
colors = ['darkblue', 'darkorange', 'lightgreen']

figPie = go.Figure(data=[go.Pie(labels=ch[0].tolist(),
                             values=ch[1].tolist())])
figPie.update_traces(hoverinfo='label+percent', textinfo='value', textfont_size=20,
                  marker=dict(colors=colors, line=dict(color='#000000', width=2)))
"""
ch = ch[ch.index != 2]

countss = []
valss =ch[1].tolist()
for c in ch[0]:
    countss.append(str(c))
countss.append("playlistCount")
valss.append(str(len(playlists_df.iloc[:,1])))
data = dict(
    counts=countss,
    values=valss)
#figChanel = px.funnel(data, x='number', y='stage')
#figCh = go.Figure(go.Funnel(y = ch[0].tolist(), x = ch[1].tolist()))
figPat = px.bar(data, x="counts", y="values", color="values",
             pattern_shape="values", pattern_shape_sequence=[".", "x", "+", "-"])

dfSome = videos_df.loc[:, ["likeCount","commentCount"]]
"""figTime = px.line(videos_df, x="publishedAt", y=dfSome.columns,
              hover_data={"publishedAt": "|%B %d, %Y"},
              title='Statistics in time')
figTime.update_xaxes(
    dtick="M1",
    tickformat="%b\n%Y")"""


figTime1 = px.line(videos_df, x='publishedAt', y="viewCount")
figTime2 = px.line(videos_df, x='publishedAt', y=dfSome.columns)

sorted_videos_by_likes_2 = videos_df.sort_values(by=['likeCount'], ascending=False)
dff = sorted_videos_by_likes.iloc[0:5, :]
colors = ['darkblue', 'darkorange', 'lightgreen']
figPie = go.Figure(data=[go.Pie(labels=dff['title'].tolist(),
                             values=dff['likeCount'].tolist())])
figPie.update_traces(hoverinfo='label+percent', textinfo='value', textfont_size=20,
                  marker=dict(colors=colors, line=dict(color='#000000', width=2)))

jumbotron = html.Div(
    dbc.Container(
        [
            html.P(
                "On the right you can see the basic statistics for the channel "  + videos_df.iloc[0,5] + ". "
                "From the scatter plot below, you can read data about all the videos of the channel. "
                "The size will represent the comment count if the comment count is non zero for each video.",
                className="lead",
            ),
            html.Hr(className="my-2"),
            html.P(
                "You can also use select menu to sort the videos and select top 5 of each category."
            ),


        ],
        fluid=True,
        className="my-3 py-1",
    ),
    className="bg-light rounded-3 my-5",
)

header1 = html.Div(
    dbc.Container(
        [
            html.H1(
                "Channel statistics for the channel " + videos_df.iloc[0,5],
                className="display-6 mx-auto",
            )
        ],
        fluid=True,
        className="my-2 py-1 d-flex",
    ),
    className="rounded-3 my-1",
)

header2 = html.Div(
    dbc.Container(
        [
            html.H1(
                "Playlist statistics for the channel " + videos_df.iloc[0,5],
                className="display-6 mx-auto",
            )
        ],
        fluid=True,
        className="my-2 py-1 d-flex",
    ),
    className="rounded-3 my-5",
)

tab1_content = dbc.Card(
    dbc.CardBody(
        [
            html.Div(
                dcc.Dropdown(
                id='demo-dropdown',
                options=[
                    {'label': 'Top 5 videos by number of likes', 'value': 'likeCount'},
                    {'label': 'Top 5 videos by number of views', 'value': 'viewCount'},
                    {'label': 'Top 5 videos by number of comments', 'value': 'commentCount'}
                ],
                value='likeCount')
            ),
            html.Div(id='dd-output-container'),
            dcc.Graph(id='graph-pie', figure=figPie)
        ]
    ),
    className="mt-3",
)

tab2_content = dbc.Card(
    dbc.CardBody(
        [
            dcc.Graph(figure=figTime1)
        ]
    ),
    className="mt-3",
)
tab3_content = dbc.Card(
    dbc.CardBody(
        [
            dcc.Graph(figure=figTime2)
        ]
    ),
    className="mt-3",
)

tabs = dbc.Tabs(
    [
        dbc.Tab(tab1_content, label="Top 5"),
        dbc.Tab(tab2_content, label="Views in time"),
        dbc.Tab(tab3_content, label="Likes and comments in time"),
    ]
)

plItems =[]
for i in range(len(playlists_df)):
    d = pd.DataFrame.from_dict(playlists_df['thumbnails'][i])
    imgUrl = d['high']['url']
    item= dbc.AccordionItem(
        dbc.Card(
            [
                dbc.Row(
                    [
                        dbc.Col(
                            dbc.CardImg(
                                src=imgUrl,
                                className="img-fluid rounded-start",
                            ),
                            className="col-md-4",
                        ),
                        dbc.Col(
                            dbc.CardBody(
                                [
                                    html.H4('Playlist name: '+playlists_df.iloc[i, 1], className="card-title"),
                                    html.P(
                                        "Description: " + playlists_df.iloc[i, 6],
                                        className="card-text",
                                    ),
                                    html.P(
                                        "Videos: " + str(playlists_df.iloc[i, 2]),
                                        className="card-text",
                                    ),
                                    html.Small(
                                        "Published at: " + playlists_df.iloc[i, 0],
                                        className="card-text text-muted",
                                    ),
                                ]
                            ),
                            className="col-md-8",
                        ),
                    ],
                    className="g-0 d-flex align-items-center",
                )
            ],
            className="mb-3",
            #style={"maxWidth": "540px"},
        ),
        title=playlists_df.iloc[i, 1]
    )
    plItems.append(item)

accordion = html.Div(
    dbc.Accordion(
        plItems,
        start_collapsed=True,
    ),
    className='my-5'
)
counter = {}
for i in range(len(playlists_df)):
    datem = str(playlists_df.iloc[i,0])[0:4]
    if datem not in counter.keys():
        counter[datem] = 1
    else:
        counter[datem] += 1

cnt = pd.DataFrame.from_dict(counter.items()).sort_values(by=0)
cnt = cnt.rename(columns={0: "Years", 1: "Playlist per year"})
plTime = px.line(cnt, x="Years", y="Playlist per year")

layout = html.Div(children=[
    #dcc.Graph(figure=figChanel),
    #dcc.Graph(figure=figPat),
    header1,
    dbc.Row(
        [dbc.Col(jumbotron, className='my-5'),
         dbc.Col(dcc.Graph(figure=figPat))],
        className="d-flex",
    ),
    dcc.Graph(
        figure=fig
    ),
    html.Div(tabs),
    header2,
    accordion,
    dcc.Graph(figure=plTime)
],
    className="container",
    #style={'background':'red'}
)

@app.callback(
    [Output('dd-output-container', 'children'),
     Output('graph-pie', 'figure')],
    Input('demo-dropdown', 'value')
)
def update_output(value):
    print(value)
    # Group data together
    global videos_df
    sorted_videos_by_value= videos_df.sort_values(by=[value], ascending=False)
    d = sorted_videos_by_value.iloc[0:5, :]
    col = ['darkblue', 'darkorange', 'lightgreen']
    f = go.Figure(data=[go.Pie(labels=d['title'].tolist(),
                                values=d[value].tolist())])
    f.update_traces(hoverinfo='label+percent', textinfo='value', textfont_size=20,
                     marker=dict(colors=col, line=dict(color='#000000', width=2)))


    return 'You have selected "{}"'.format(value), f