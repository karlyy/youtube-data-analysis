# YouTube data analysis

Data analysis in Python and Dash Plotly

## About the application

- [ ] Page 1: A video downloader using the package pytube. 
The user enters a video link, the video ID is fetched from the 
URL with urlparse and the video is downloaded in the root directory. Basic statistics 
for the video are displayed. 
- [ ] Page 2: Displaying the statistics for the YouTube channel of the downloaded video.

## Run the application
Clone the repository.
Execute the command line: python index.py



## Test the application
Enter a url of a YouTube video on the Downloader page. 
Restart the application to see the results on the Channel statistics page.



