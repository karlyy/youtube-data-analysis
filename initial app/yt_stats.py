import json
from tqdm import tqdm
import requests

class YTstats:

    def __init__(self, api_key, channel_id=None):
        self.api_key = api_key
        self.channel_id = channel_id
        self.channel_statistics = None
        self.video_data = None
        self.playlist_data = None

    def get_channel_statistics(self):
        url = f'https://www.googleapis.com/youtube/v3/channels?part=statistics&id={self.channel_id}&key={self.api_key}'
        #print(url)
        json_url = requests.get(url)
        data = json.loads(json_url.text)
        #print(data)

        try:
            data = data['items'][0]['statistics']
        except KeyError:
            print('Could not get channel statistics')
            data = {}

        self.channel_statistics = data
        return data

    def get_channel_video_data(self):
        #get video ids
        channel_videos, channel_playlists = self._get_channel_content(limit=50)
        #print(channel_videos)
        #get video stats
        parts=["snippet", "statistics", "contentDetails", "topicDetails"]
        for video_id in tqdm(channel_videos):
            for part in parts:
                data = self.get_single_video_data(video_id, part)
                channel_videos[video_id].update(data)
        self.video_data = channel_videos
        #self.playlist_data = channel_playlists
        #print(channel_playlists)
        return channel_videos

    def get_single_video_data(self, video_id, part):
        url = f"https://www.googleapis.com/youtube/v3/videos?part={part}&id={video_id}&key={self.api_key}"
        json_url = requests.get(url)
        data = json.loads(json_url.text)  # dictionary
        try:
            data = data["items"][0][part]
        except:
            print("error")
            data = dict()
        return data

    def get_channel_playlist_data(self):
        channel_videos, channel_playlists = self._get_channel_content(limit=50)
        parts = ["contentDetails", "id", "player", "snippet", "status"]
        #print("chanel playlists:")
        #print(channel_videos)
        for playlist_id in tqdm(channel_playlists):
            for part in parts:
                if part == 'id':
                    data = {'id': self._get_single_playlist_data(playlist_id, part)}
                    print(data)
                else:
                    data = self._get_single_playlist_data(playlist_id, part)
                    print(data)
                #print("single playlist data for part " + part)
                #print(data)
                channel_playlists[playlist_id].update(data)
                print(channel_playlists[playlist_id])
        self.playlist_data = channel_playlists
        print("ALL PLAYLISTS")
        print(channel_playlists)
        return channel_playlists

    def _get_single_playlist_data(self, playlist_id, part):
        url = f"https://www.googleapis.com/youtube/v3/playlists?part={part}&id={playlist_id}&key={self.api_key}"

        json_url = requests.get(url)
        data = json.loads(json_url.text)  # dictionary
        try:
            data = data["items"][0][part]
        except:
            print("error")
            data = dict()
        return data


    def _get_channel_content(self, limit=None, check_all_pages=True):
        """
        Extract all videos and playlists, can check all available search pages
        channel_videos = videoId: title, publishedAt
        channel_playlists = playlistId: title, publishedAt
        return channel_videos, channel_playlists
        """
        url = f"https://www.googleapis.com/youtube/v3/search?key={self.api_key}&channelId={self.channel_id}&part=snippet,id&order=date"
        if limit is not None and isinstance(limit, int):
            url += "&maxResults=" + str(limit)

        vid, pl, npt = self._get_channel_content_per_page(url)
        idx = 0
        while(check_all_pages and npt is not None and idx < 10):
            nexturl = url + "&pageToken=" + npt
            next_vid, next_pl, npt = self._get_channel_content_per_page(nexturl)
            vid.update(next_vid)
            pl.update(next_pl)
            idx += 1

        return vid, pl

    def _get_channel_content_per_page(self, url):
        """
        Extract all videos and playlists per page
        return channel_videos, channel_playlists, nextPageToken
        """
        json_url = requests.get(url)
        data = json.loads(json_url.text)
        channel_videos = dict()
        channel_playlists = dict()
        if 'items' not in data:
            print('Error! Could not get correct channel data!\n', data)
            return channel_videos, channel_videos, None

        nextPageToken = data.get("nextPageToken", None)

        item_data = data['items']
        for item in item_data:
            try:
                kind = item['id']['kind']
                published_at = item['snippet']['publishedAt']
                title = item['snippet']['title']
                if kind == 'youtube#video':
                    video_id = item['id']['videoId']
                    channel_videos[video_id] = {'publishedAt': published_at, 'title': title}
                elif kind == 'youtube#playlist':
                    playlist_id = item['id']['playlistId']
                    channel_playlists[playlist_id] = {'publishedAt': published_at, 'title': title}
            except KeyError as e:
                print('Error! Could not extract data from item:\n', item)

        return channel_videos, channel_playlists, nextPageToken


    def dump(self):
        if self.channel_statistics is None or self.video_data is None:
            print("Data is none")
            return

        fused_data = {self.channel_id:
                          {"channel_statistics": self.channel_statistics,
                           "video_data": self.video_data,
                           "playlist_data": self.playlist_data}
                      }

        channel_title = self.video_data.popitem()[1].get("channelTitle", self.channel_id)
        channel_title = channel_title.replace(" ", "_").lower()
        file_name = "../datasets/youtube.json"
        with open(file_name, "w") as f:
            json.dump(fused_data, f, indent=4)

        print('file dumped')